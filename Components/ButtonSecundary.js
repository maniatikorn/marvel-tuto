import React from 'react';

export default function ButtonSecundary({ children }) {
  return (
    <>
      <button>
        {children}
      </button>
      <style jsx>{`
          button {
            width:360px;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 10px;
            height: 66px;
            font-size: 18px;
            background: #0074FA;
            color: #fff;
            font-weight:  bold;
            border-radius: 10px;
            border: none;
            margin-top: 15px;
            cursor: pointer;
            transition: opacity .3s ease;
          }
          button:hover {
            opacity: .7;
          }
          @media screen and (max-width: 390px) {
            button {
              width: 100%;
            }
          }
      `}
      </style>
    </>
  )
}