import { useState } from 'react';
import Link from 'next/link'

import useAuth from '../hooks/useAuth';

const NavBar = () => {
  const { user } = useAuth();
  const [showLogout, setShowLogout] = useState(false);
  return (
    <nav className="navbar">
      <div>
        <Link href="/">
          <img src="/logo.svg" alt="Logo" className="navbar__logo"/>
        </Link>
      </div>
      <div>
        <Link href="/favorites">
          <img src="/like.png" alt="Icon like" />
        </Link>
        <img onClick={() => setShowLogout(true)} src="/menu-logo.png" alt="Icon-menu" />
        { user === null ? <p><a href="/login">Iniciar sesión</a></p> : <p>{user?.displayName}</p>}
        <div>
        { user === null ? <img src="https://cdn.pixabay.com/photo/2021/05/16/07/26/naruto-6257398_960_720.png" alt="Image avatar" /> : <img src={user?.photoURL} alt="Image avatar" />}
        </div>
      </div>
      {
        showLogout &&
        <Link href="/login"><button onClick={() => setShowLogout(false)}>Logout</button></Link>
      }
    </nav>
  )
}

export default NavBar